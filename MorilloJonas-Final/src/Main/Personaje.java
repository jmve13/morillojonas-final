package Main;

import java.util.ArrayList;

import Core.Sprite;

/**
 * Clase de declaracion del personaje principal
 * 
 * @author Jonas
 *
 */
public class Personaje extends Sprite {
	/**
	 * Variable global que define la velocidad de nuestro personaje, tambien es
	 * utilizada para definir a la velocidad que se movera el scroll
	 */
	int spd = 3;
	/**
	 * Variable que indica en que direccion esta mirando el personaje
	 */
	int face = 2;

	/**
	 * Lista donde se almacena el equipo de nuestro personaje.
	 */
	ArrayList<String> equip = new ArrayList<String>();

	// Constructor.
	public Personaje(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = true;
		this.terrain = false;
		this.trigger = false;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Funciones de movimiento de personaje, en cada funcion se cambia la direccion
	 * en la que se mueve nuestro personaje, tambien cambia la direccion en la que
	 * mira el mismo
	 */
	public void moveIzq() {
		this.setVelocity(-spd, velocity[1]);
		//x1 -= spd;
		//x2 -= spd;
		// direccion='a';
		face = 4;
	}

	public void moveDer() {
		// TODO Auto-generated method stub
		this.setVelocity(spd, velocity[1]);
		//x1 += spd;
		//x2 += spd;
		// direccion='d';
		face = 3;
	}

	public void moveArr() {
		this.setVelocity(velocity[0], -spd);
		//y1 -= spd;
		//y2 -= spd;
		// direccion='w';
		face = 1;

	}

	public void moveAba() {
		
		this.setVelocity(velocity[0], spd);
		//y1 += spd;
		//y2 += spd;
		// direccion='s';
		face = 2;

	}

	/**
	 * Funcion dibde se comprueba si tienes las botas en la lista de equipo y si las
	 * tienes aumenta tu velocidad cuando es ejecutada.
	 */

	public void run() {
		// TODO Auto-generated method stub
		if (equip.contains("boots")) {
			spd = 6;
		} else {
			System.out.println("Sin botas no corres");
		}

	}

	/**
	 * Funcion donde se reestablece la velocidad de movimiento de nuestro personaje
	 * a su valor original
	 */
	public void noRun() {
		// TODO Auto-generated method stub
		if (equip.contains("boots")) {
			spd = 3;
		}
	}

	/**
	 * Funciones donde se cambia la variable que determina hacia que direccion mira
	 * el persoanje
	 */
	public void faceUp() {
		face = 1;
	}

	public void faceDown() {
		face = 2;
	}

	public void faceRight() {
		face = 3;
	}

	public void faceLeft() {
		face = 4;
	}

	public void espada() {
		// TODO Auto-generated method stub

	}

	/**
	 * Funcion donde se decide mediante un switch en que posicion aparecera la
	 * flecha dependiendo de hacia que direccion estemos mirando.
	 * 
	 * @return devuelve el sprite del proyectil que mas adelante sera a�adido a una lista
	 */
	public flecha shoot() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		switch (face) {
		// Dispara arriba
		case (1): {
			flecha m = new flecha("flecha", x1 + 10, y1 - 60, x2 - 20, y2 - 60, 0, "flecha1.png");
			m.move();
			return m;
		}
		// Dispara abajo
		case (2): {
			flecha m = new flecha("flecha", x1 + 10, y1 + 60, x2 - 20, y2 + 60, 0, "flecha2.png");
			m.move();
			return m;
		}
		// Dispara derecha S
		case (3): {
			flecha m = new flecha("flecha", x1 + 60, y1 + 10, x2 + 60, y2 - 20, 0, "flecha3.png");
			m.move();
			return m;
		}
		// Dispara izquierda
		case (4): {
			flecha m = new flecha("flecha", x1 - 60, y1 + 10, x2 - 60, y2 - 20, 0, "flecha4.png");
			m.move();
			return m;
		}
		default: {
			flecha m = new flecha("flecha", x1 + 60, y1 + 10, x2 + 60, y2 - 20, 0, "flecha.png");
			m.move();
			return m;
		}
		}

	}
	// public Hitbox pegar() {
	// Hitbox h = new Hitbox("hitbox", x2, (y1+y2)/2, x2+50, ((y1+y2)/2)+50, 0);
	// return h;
	// }

	public void donotmove() {
		// TODO Auto-generated method stub
		this.setVelocity(0, 0);
		
		
	}
}
