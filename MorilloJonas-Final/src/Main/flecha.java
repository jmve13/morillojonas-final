package Main;

import Core.Sprite;

/**
 * Clase de declaracion de los proyectiles que dispara nuestro personaje
 * @author Jonas
 *
 */

public class flecha extends Sprite {

	public flecha(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		// TODO Auto-generated constructor stub
		this.physicBody = true;
		this.trigger = true;
	}

	/**
	 * Funciones de movimiento del proyectil de nuestro personaje, indica em que
	 * direccion se mueve la flecha y a que velocidad va.
	 */
	public void move() {
		switch (Main.pj.face) {
		case (1): {
			this.setVelocity(0, -3);
			break;
		}
		case (2): {
			this.setVelocity(0, 3);
			break;
		}
		case (3): {
			this.setVelocity(3, 0);
			break;
		}
		case (4): {
			this.setVelocity(-3, 0);
			break;
		}
		default: {
			System.out.println("error");
			break;
		}
		}

	}

	/**
	 * Funcion para comprobar la colision de los proyectiles con el enemigo, con
	 * cada colision se restara 3 a la variable global que representa la vida de
	 * nuestro enemigo y despues de bajarle la vida se ejecuta la funcion para
	 * comprobar que la vida de nuestro enemigo sigue siendo superior a 0.
	 * Por ultimo borrara el sprite de la flecha que ha colisionado
	 */
	public void collision() {

		if (this.collidesWith(Main.en1)) {
			System.out.println("pum");
			Main.en1.hp = Main.en1.hp - 3;
			System.out.println(Main.en1.hp);
			Main.en1.checkdmg();

			this.delete();
		}

	}

}
