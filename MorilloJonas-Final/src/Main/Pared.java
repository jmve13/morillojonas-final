package Main;
import Core.Sprite;

/**
 * Clase para declarar elementos del escenario (Por implementar)
 * @author Jonas
 *
 */

public class Pared extends Sprite {
		
		public Pared(String name, int x1, int y1, int x2, int y2, String path) {
			super(name, x1, y1, x2, y2, path);
			terrain=true;
			solid=true;
			
		}

}
