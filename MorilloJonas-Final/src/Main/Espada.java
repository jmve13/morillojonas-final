package Main;

import Core.Sprite;

/**
 * Clase de declaracion de la hitbox de ataque fisico del personaje
 * @author Jonas
 *
 */
public class Espada extends Sprite {
	
	public Espada(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		/**
		 * Pendiente de implementar como hitbox de ataque fisico de nuestro personaje
		 */
		
	}

}