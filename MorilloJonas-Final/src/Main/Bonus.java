package Main;

import Core.Sprite;

/**
 * Clase de declaracion de bonus para el personaje
 * 
 * @author Jonas
 *
 */
public class Bonus extends Sprite {

	public Bonus(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.trigger=true;

	}

	/**
	 * Funcion donde se combrueba la colision de el personaje con las botas y una
	 * vez se ha colisionado se a�aden a una lista donde se almacena el equipo de
	 * nuestro personaje y se elimina el sprite
	 */
	public static void pjcollision() {
		// TODO Auto-generated method stub
		if (Main.pj.collidesWith(Main.boots)) {
			Main.pj.equip.add("boots");
			Main.boots.delete();
			System.out.println("Ya tienes botas");
		}

	}

}
