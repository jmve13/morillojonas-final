package Main;

import Core.Field;

import Core.Sprite;
import Core.Window;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Main de el videojuego, donde se ejecuta el bucle principal
 * @author Jonas
 *
 */
public class Main {

	/**
	 * 
	 */
	static Random r = new Random();
	static Field f = new Field();
	static Window w = new Window(f);
	/**
	 * Sprite del personaje
	 */
	static Personaje pj = new Personaje("Personaje", 50, 225, 100, 275, "Sprite.png");
	/**
	 * Sprite del enemigo
	 */
	static Enemigo en1 = new Enemigo("Enemigo1", 300, 300, 350, 350, "enemigo1.png");
	/**
	 * Sprite de un a pared (aun no implementado)
	 */
	static Pared ar1 = new Pared("arbol1", 50, 50, 100, 100, "arbolito.png");
	/**
	 * Arraylist donde se almacenan los proyectiles lanzados por nuestro personaje
	 */
	static ArrayList<flecha> m = new ArrayList<flecha>();
	/**
	 * Sprite del bonus de las botas de velocidad
	 */
	static Bonus boots = new Bonus("Bonus", 100, 70, 120, 100, "boots.png");
	/**
	 * Sprite de el mapa de el juego
	 */
	static Map mp = new Map("Fondo", 0, 0, 1000, 750, "Suelo1.png");

	/**
	 * Variable global: Contador para el movimiento del enemigo
	 */
	static int contmoven = 10;

	/**
	 * Variables globales para el scroll
	 */
	static int minscrollx = 0;
	static int maxscrollx = 700;
	static int minscrolly = 0;
	static int maxscrolly = 400;

	public static void main(String[] args) throws InterruptedException {

		// TODO Auto-generated method stub
		// boolean flag = true;

		ArrayList<Sprite> sprites = new ArrayList<>();
		sprites.add(mp);
		sprites.add(pj);
		sprites.add(boots);
		sprites.add(en1);
		sprites.add(ar1);
		while (true) {

			sprites.addAll(m);

			/**
			 * Funcion de acciones del personaje
			 */
			input();

			/**
			 * Funcion para el movimiento del enemigo
			 */

			moven();

			/**
			 * Funcion para comprobar la colision del personaje con las botas de velocidad
			 */

			Bonus.pjcollision();

			/**
			 * Bucle con funcion para ver las colisiones los proyectiles con el enemigo
			 */
			for (int i = 0; i < m.size(); i++) {
				m.get(i).collision();
			}

			f.draw(sprites);

			// pausa de 33 ms

			Thread.sleep(33);

		}

	}

	/**
	 * Funcion para el movimiento del enemigo, el cual se mueve aleatoriamente cada
	 * vez que la variable global contmoven llega a 10, suma 1 con cada vez que el
	 * bucle principal se completa y vuelve a cero cada vez que alcanza 10
	 */
	private static void moven() {
		// TODO Auto-generated method stub
		if (contmoven == 10) {
			int mov = r.nextInt(4);
			/**
			 * Switch donde se escoge el siguiente movimiento del enemigo de forma aleatoria
			 */
			switch (mov) {
			case (0): {
				en1.emoveAba();
				break;
			}
			case (1): {
				en1.emoveArr();
				break;
			}
			case (2): {
				en1.emoveDer();
				break;
			}
			case (3): {
				en1.emoveIzq();
				break;

			}
			}
			contmoven = 0;
		} else {
			contmoven++;
		}

	}

	/**
	 * Funcion donde se leen las teclas pulsadas y donde se llama a la funcion
	 * correspondiente dependiendo de la tecla que se este pulsando
	 */
	private static void input() {
		// TODO Auto-generated method stub
		HashSet<Character> list = (HashSet<Character>) w.getPressedKeys();

		// System.out.println(list);
		// MOVIMIENTO
		/**
		 * Lectura de teclas para el movimiento del personaje, cada una de las
		 * direcciones modifica el scroll de manera que el personaje siempre esta
		 * situado en el centro
		 */
		if (list.contains('d')) {
			pj.moveDer();
			if (pj.x1 < maxscrollx) {
				// scroll positiu: A la dreta.
				f.scroll(-pj.spd, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				maxscrollx += pj.spd;
				minscrollx += pj.spd;
			}

		}

		if (list.contains('a')) {
			pj.moveIzq();
			if (pj.x1 < minscrollx) {
				// scroll positiu: A la dreta.
				f.scroll(pj.spd, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscrollx -= pj.spd;
				maxscrollx -= pj.spd;
			}

		}
		if (list.contains('w')) {
			pj.moveArr();
			if (pj.y1 < minscrolly) {
				// scroll positiu: A la dreta.
				f.scroll(0, pj.spd);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscrolly -= pj.spd;
				maxscrolly -= pj.spd;
			}
		}
		if (list.contains('s')) {
			pj.moveAba();
			if (pj.y1 < maxscrolly) {
				// scroll positiu: A la dreta.
				f.scroll(0, -pj.spd);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				maxscrolly += pj.spd;
				minscrolly += pj.spd;
			}
		}
		if(!list.contains('a') && !list.contains('w') && !list.contains('s') && !list.contains('d')) {
			pj.donotmove();
			
		}
		
		/**
		 * Funciones para que mientras se este pulsando la barra espaciadora el
		 * personaje aumente su velocidad al moverse y cuando se deje de pulsar el
		 * espacio vuelva a su velocidad original
		 */

		// CORRER
		if (list.contains(' ')) {
			pj.run();
		} else {
			pj.noRun();
		}
		// MIRAR
		/**
		 * Funciones para determinar hacia que lado dirige sis acciones el personaje.
		 * Determinara en que direccion salen las flechas de nuestro personaje
		 */
		if (list.contains('i')) {
			// Arriba
			pj.faceUp();
		}
		if (list.contains('j')) {
			// Izquierda
			pj.faceLeft();
		}
		if (list.contains('l')) {
			// Derecha
			pj.faceRight();
		}
		if (list.contains('k')) {
			// Abajo
			pj.faceDown();
		}
		// DISPARAR
		/**
		 * Funcion para a�adir disparos a la lista donde se almacena cada uno
		 */
		if (w.getKeysDown().contains('q')) {
			m.add(pj.shoot());

		}

		System.out.println(pj.face);

	}
}
