package Main;
import Core.Sprite;

/**
 * Clase para declarar el mapa del juego
 * @author Jonas
 *
 */

public class Map  extends Sprite {
	public Map(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = false;
		this.terrain = false;
		this.trigger = false;
		
	}
}
