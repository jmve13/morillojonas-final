package Main;

import Core.Sprite;


/**
 * Clase de declaracion del enemigo basico
 * @author Jonas
 *
 */
public class Enemigo extends Sprite {
	/**
	 * Variable que determina la vida de nuestro enemigo
	 */
	public int hp = 10;

	public Enemigo(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		this.physicBody = true;

	}

	/**
	 * Funcion donde se combrueba que la vida de nuestro enemigo sigue siendo mayor
	 * que 0
	 */
	public void checkdmg() {
		if (this.hp <= 0) {
			Main.en1.delete();
		}
	}

	/**
	 * Funciones de movimiento del enemigo, en cada uno determina la velocidad a la
	 * que se movera nuestro enemigo y hacia que direccion
	 */
	public void emoveIzq() {
		this.setVelocity(-3, 0);
		// direccion='a';
	}

	public void emoveDer() {
		// TODO Auto-generated method stub
		this.setVelocity(3, 0);
		// direccion='d';
	}

	public void emoveArr() {
		this.setVelocity(0, -3);
		// direccion='w';

	}

	public void emoveAba() {
		this.setVelocity(0, 3);
		// direccion='s';

	}

}
